module. exports = {
    HOST: "localhost",
    USER: "kavya",
    PASSWORD: "password",
    DB: "testdb",
    dialect: "postgres",
    pool: {
        max:6,
        min:0,
        acquire: 30000,
        idle: 10000
    }
};