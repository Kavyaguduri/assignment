const db =require("../models");
const Tutorial= db.tutorials;
const Op= db.Sequelize.Op;
exports.create= (req,res)=>{
    if(!req.body.title){
        res.status(400).send({
            message: "empty"
        });
        return;
    }
    const tutorial={
        title: req.body.title,
        description:req.body.description,
        published:req.body.published? req.body.published: false
    };
    Tutorial.create(tutorial)
    .then(data=>{
        res.send(data);
    })
    .catch(err=> {
        res.status(500).send({
            message:
            err.message|| "error"
        });
    });


};
exports.delete=(req, res)=> {
    const id=req.params.id;
    Tutorial.destroy({
        where:{id:id}
    })
    .then(num=> {
        if(num==1){
            res.send({
                message: "deleted"
            });
        } else {
            res.send({
                message: "can not delete"
            });
        }
    })
    .catch(err=>{
        res.status(500).send({
            message: "error"
        });
    });

};
exports.update=(req, res)=> {
    const id=req.params.id;
    Tutorial.update(req.body,{
        where :{id:id}
    })
    .then(num=>{
        if(num==1){
            res.send({
                message:"updated"
            });
        } else {
            res.send({
                message: "can not update "
            });
        }
    })
    .catch(err=>{
        res.status(500).send({
            message: "error"
        });
    });

};
